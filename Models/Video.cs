﻿using System.Collections.Generic;

namespace MindGeekTest.Models
{
	public class Video
	{
		public string Title { get; set; }

		public string Type { get; set; }

		public string Url { get; set; }

		public IEnumerable<VideoAlternative> Alternatives { get; set; }
	}
}