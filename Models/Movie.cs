﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace MindGeekTest.Models
{
	public class Movie
	{
		public string Id { get; set; }

		public string Headline { get; set; }

		public string Quote { get; set; }

		public string Synopsis { get; set; }

		public int Year { get; set; }

		public int Duration { get; set; }

		public IEnumerable<Person> Cast { get; set; }

		public IEnumerable<Person> Directors { get; set; }

		public string Cert { get; set; }

		public string Class { get; set; }

		public IEnumerable<string> Genres { get; set; }

		public DateTime LastUpdated { get; set; }

		public IEnumerable<Image> CardImages { get; set; }

		public IEnumerable<Gallery> Galleries { get; set; }

		public IEnumerable<Image> KeyArtImages { get; set; }

		public IEnumerable<Video> Videos { get; set; }

		public ViewingWindow ViewingWindow { get; set; }

		public string Url { get; set; }

		public string SkyGoId { get; set; }

		public string SkyGoUrl { get; set; }

		public string Body { get; set; }

		public int Rating { get; set; }

		public string ReviewAuthor { get; set; }

		public string Sum { get; set; }
	}
}
