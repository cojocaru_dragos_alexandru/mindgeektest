﻿namespace MindGeekTest.Models
{
	public class Image
	{
		public string Url { get; set; }

		public int H { get; set; }

		public int W { get; set; }
	}
}