﻿using System;

namespace MindGeekTest.Models
{
	[Serializable]
	public class CachedMedia
	{
		public string ContentType { get; set; }

		public byte[] Content { get; set; }
	}
}