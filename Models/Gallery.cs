﻿using System;

namespace MindGeekTest.Models
{
	public class Gallery
	{
		public string Title { get; set; }

		public string Url { get; set; }

		public string ThumbnailUrl { get; set; }

		public string Id { get; set; }
	}
}