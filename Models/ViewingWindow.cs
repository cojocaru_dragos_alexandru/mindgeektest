﻿using System;

namespace MindGeekTest.Models
{
	public class ViewingWindow
	{
		public DateTime StartDate { get; set; }

		public DateTime EndDate { get; set; }

		public string WayToWatch { get; set; }
	}
}