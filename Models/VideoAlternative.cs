﻿namespace MindGeekTest.Models
{
	public class VideoAlternative
	{
		public string Quality { get; set; }

		public string Url { get; set; }
	}
}