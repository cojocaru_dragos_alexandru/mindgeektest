﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using MindGeekTest.Models;
using Newtonsoft.Json;

namespace MindGeekTest.Services
{
	public interface IMovieCollectionService
	{
		Task<(IEnumerable<Movie> movies, int pageCount)> GetMovies(int pageNumber = 1, int itemsPerPage = 5);
	}

	public class MovieCollectionService : IMovieCollectionService
	{
		private readonly IHttpClientFactory _clientFactory;
		private readonly IConfiguration _configuration;

		public MovieCollectionService(IConfiguration configuration, IHttpClientFactory clientFactory)
		{
			_configuration = configuration;
			_clientFactory = clientFactory;
		}

		public async Task<(IEnumerable<Movie> movies, int pageCount)> GetMovies(int pageNumber = 1, int itemsPerPage = 5)
		{
			if (pageNumber <= 0)
				pageNumber = 1;

			if (itemsPerPage < 1)
				itemsPerPage = 1;
			else if (itemsPerPage > 10)
				itemsPerPage = 10;

			try
			{
				var httpClient = _clientFactory.CreateClient(_configuration["DataBaseUrl"]);
				var response = await httpClient.GetStringAsync("/files/showcase.json");

				var movies = JsonConvert.DeserializeObject<IEnumerable<Movie>>(response.Replace(_configuration["DataBaseUrl"], ""));

				var result = movies
					.OrderBy(x => x.Headline)
					.Skip((pageNumber - 1) * itemsPerPage)
					.Take(itemsPerPage);

				return (result, movies.Count() / itemsPerPage);
			}
			catch (Exception ex)
			{
				return (new List<Movie>(), 0);
			}
		}
	}
}
