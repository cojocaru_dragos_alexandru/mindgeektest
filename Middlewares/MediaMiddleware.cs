﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using MindGeekTest.Extensions;
using MindGeekTest.Models;

namespace MindGeekTest.Middlewares
{
	public class MediaMiddleware
	{
		private readonly IHttpClientFactory _clientFactory;
		private readonly RequestDelegate _next;
		private readonly IConfiguration _configuration;
		private readonly IDistributedCache _cache;
		private readonly string _mediaBaseUrl;

		public MediaMiddleware(RequestDelegate next, IConfiguration configuration, IDistributedCache cache, IHttpClientFactory clientFactory)
		{
			_next = next ?? throw new ArgumentNullException(nameof(next));
			_configuration = configuration;
			_cache = cache;
			_clientFactory = clientFactory;
			_mediaBaseUrl = configuration["DataBaseUrl"];

		}

		public async Task Invoke(HttpContext context)
		{
			if (context == null) throw new ArgumentNullException(nameof(context));
			if (context.Request == null) throw new ArgumentNullException(nameof(context.Request));

			var mediaUrl = context.Request.Path.Value;

			// If the user is requesting images, serve them from cache whenever possible
			if (mediaUrl.StartsWith("/images") && HttpMethods.IsGet(context.Request.Method))
			{
				var cachedMedia = await _cache.GetAsync<CachedMedia>(mediaUrl);

				if (cachedMedia == null)
				{
					// Forward the call to the target address
					var proxiedResponse = await SendProxyHttpRequest(context, $"{_mediaBaseUrl}{mediaUrl}");

					context.Response.StatusCode = (int)proxiedResponse.StatusCode;

					//foreach (var (key, value) in proxiedResponse.Headers)
					//	context.Response.Headers[key] = value.ToArray();

					//foreach (var (key, value) in proxiedResponse.Content.Headers)
					//	context.Response.Headers[key] = value.ToArray();

					//context.Response.Headers.Remove("transfer-encoding");

					// Read the data we got from the target address
					await using var responseStream = await proxiedResponse.Content.ReadAsStreamAsync();

					// Copy the read data to a local byte array variable
					await using var ms = new MemoryStream();
					responseStream.CopyTo(ms);

					if (proxiedResponse.IsSuccessStatusCode)
					{
						// Create the object we're going to store in our cache
						cachedMedia = new CachedMedia
						{
							ContentType = proxiedResponse.Content.Headers.ContentType.MediaType,
							Content = ms.ToArray()
						};

						// Cache the response type and content
						await _cache.SetAsync(mediaUrl, cachedMedia, new DistributedCacheEntryOptions
						{
							AbsoluteExpiration = proxiedResponse.Content.Headers.Expires ?? DateTimeOffset.UtcNow.AddDays(1)
						});
					}

					// Copy the response we got from the target back onto the user's response
					ms.Seek(0, SeekOrigin.Begin);
					await ms.CopyToAsync(context.Response.Body, 81920, context.RequestAborted);
				}
				else
				{
					// Replace the response with the content we've got in our cache
					context.Response.ContentType = cachedMedia.ContentType;
					context.Response.ContentLength = cachedMedia.Content.Length;

					cachedMedia.Content.CopyTo(context.Response.BodyWriter.GetMemory(cachedMedia.Content.Length).Span);
					context.Response.BodyWriter.Advance(cachedMedia.Content.Length);

					await context.Response.BodyWriter.FlushAsync();
				}
			}

			await _next(context);
		}
		
		private Task<HttpResponseMessage> SendProxyHttpRequest(HttpContext context, string proxiedAddress)
		{
			var proxiedRequest = CreateProxyHttpRequest(context, proxiedAddress);

			var httpClient = _clientFactory.CreateClient(_configuration["DataBaseUrl"]);

			return httpClient.SendAsync(proxiedRequest, HttpCompletionOption.ResponseHeadersRead, context.RequestAborted);
		}

		private static HttpRequestMessage CreateProxyHttpRequest(HttpContext context, string uriString)
		{
			var request = context.Request;
			var requestMessage = new HttpRequestMessage();

			// Forward the request headers
			foreach (var (key, value) in request.Headers)
				if (!requestMessage.Headers.TryAddWithoutValidation(key, value.ToArray()))
					requestMessage.Content?.Headers.TryAddWithoutValidation(key, value.ToArray());

			var uri = new Uri(uriString);
			requestMessage.Headers.Host = uri.Authority;
			requestMessage.RequestUri = uri;

			requestMessage.Method = new HttpMethod(request.Method);

			return requestMessage;
		}

	}
}
