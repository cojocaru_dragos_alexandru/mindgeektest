﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Distributed;

namespace MindGeekTest.Extensions
{
	public static class DistributedCachingExtensions
	{
		public static async Task SetAsync<T>(this IDistributedCache distributedCache, string key, T value, DistributedCacheEntryOptions options, CancellationToken cancellationToken = default)
		{
			if (value == null) throw new ArgumentNullException(nameof(value));

			await distributedCache.SetAsync(key, value.ToByteArray(), options, cancellationToken);
		}

		public static async Task<T> GetAsync<T>(this IDistributedCache distributedCache, string key, CancellationToken cancellationToken = default) where T : class
		{
			var result = await distributedCache.GetAsync(key, cancellationToken);

			try
			{
				return result?.FromByteArray<T>();
			}
			catch (Exception e)
			{
				// Most probably unable to deserialize the cached object
				return null;
			}
		}
	}
}
