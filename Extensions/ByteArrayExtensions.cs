﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace MindGeekTest.Extensions
{
	public static class ByteArrayExtensions
	{
		public static byte[] ToByteArray(this object obj)
		{
			if (obj == null) return null;

			var binaryFormatter = new BinaryFormatter();

			using var memoryStream = new MemoryStream();
			binaryFormatter.Serialize(memoryStream, obj);

			return memoryStream.ToArray();
		}

		public static T FromByteArray<T>(this byte[] byteArray) where T : class
		{
			if (byteArray == null) return null;

			var binaryFormatter = new BinaryFormatter();
			using var memoryStream = new MemoryStream(byteArray);

			return binaryFormatter.Deserialize(memoryStream) as T;
		}
	}
}