﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MindGeekTest.Models;
using MindGeekTest.Services;

namespace MindGeekTest.Pages
{
	public class IndexModel : PageModel
	{
		private readonly ILogger<IndexModel> _logger;
		private readonly IMovieCollectionService _movieCollectionService;

		public IEnumerable<Movie> Movies { get; set; }

		[BindProperty(SupportsGet = true)]
		public int CurrentPage { get; set; } = 1;

		public int PageCount { get; set; } = 1;

		public bool ShowFirst => CurrentPage != 1;

		public bool ShowPrevious => CurrentPage > 1;

		public bool ShowNext => CurrentPage < PageCount;

		public bool ShowLast => CurrentPage != PageCount;

		public IEnumerable<SelectListItem> Pages =>
			Enumerable.Range(1, PageCount)
				.Select(x => new SelectListItem
				{
					Value = x.ToString(),
					Text = $"Go to page {x}"
				});

		public IndexModel(ILogger<IndexModel> logger, IMovieCollectionService movieCollectionService)
		{
			_logger = logger;
			_movieCollectionService = movieCollectionService;
		}

		public async Task OnGetAsync()
		{
			(Movies, PageCount) = await _movieCollectionService.GetMovies(CurrentPage, 9);
		}

		public async Task OnPostAsync()
		{
			(Movies, PageCount) = await _movieCollectionService.GetMovies(CurrentPage, 9);
		}
	}
}
