﻿$(document).ready(() =>
{
	// If the videos fail to load, remove the video element altogether from the DOM
	$('video').each((i, video) =>
	{
		$(video).children('source').last().on('error', error =>
		{
			$(error.currentTarget).parent().addClass('unavailable');
		});
	});

	$('#photoModal').on('show.bs.modal', event =>
	{
		const button = $(event.relatedTarget); // Button that triggered the modal

		const modal = $('.modal-body');
		const pictureSources = button.data('sources');

		//const imageSizes = Object.keys(pictureSources);

		//let sources = '';
		//imageSizes.forEach(key =>
		//{
		//	sources += `<source media="(min-width: ${key}px)" srcset="${pictureSources[key]}">`;
		//});

		//// Add the sources to the picture element
		//modal.html($(`<picture>${sources}<img src = "${pictureSources[768]}" /></picture>`));

		modal.html($(`<img src = "${pictureSources[768]}" />`));
	});
});